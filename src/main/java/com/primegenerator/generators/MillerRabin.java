package com.primegenerator.generators;

import java.math.BigInteger;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

public class MillerRabin implements PrimeGenerator {
    /**
     * Improvement over the BruteForce method by using a faster primality check
     * Generates a list of primes in the range between s, e by looping over all numbers in that range
     * and checking if a number is prime or not using the miller-rabin probabilistic primality test
     * @param s : start of the range
     * @param e : end of the range
     * @return a list of primes found in range [s - e] if any, empty list otherwise
     */
    @Override
    public List<Long> generatePrimeInRange(long s, long e) {
        List<Long> result = new LinkedList<>();
        if(s > e || e <0 || s < 0) {
            return result;
        }

        for (long i = s; i <= e; i++) {
            if (isPrime(BigInteger.valueOf(i), 128)) {
                result.add(i);
            }
        }
        return result;
    }

    /**
     * @param n suspected number
     * @param k accuracy parameter
     * @return boolean
     *  TRUE: n is prime with a probability of uncertainty of 1/2^k
     *  FALSE: n is a composite number with 100% certainty
     */
    private boolean isPrime(BigInteger n, int k) {
        // default of k for now is 128
        // trivial cases
        if (n.compareTo(BigInteger.ONE) <= 0) { // n <= 1
            return false;
        } else if (n.compareTo(BigInteger.valueOf(3)) <= 0) { // n <= 3, (2, 3 are primes)
            return true;
        }
        // (n & 1 == 0) or n % 2 == 0, check if n is even as even numbers can't be prime
        else if ( (n.and(BigInteger.ONE).compareTo(BigInteger.ZERO)) == 0) {
            return false;
        }

        // write n-1 in the form (2 ^ r) * d => n-1 = (2^r) * d
        BigInteger d = n.subtract(BigInteger.ONE);
        int r = 0;

        // reduce n-1 to the form of 2^r * d
        while( (d.and(BigInteger.ONE)).compareTo(BigInteger.ZERO) == 0) { // d is even
            r++;
            d = d.shiftRight(1); // d = d / 2
        }

        // repeat the miller-rabin test for K times
        for(int i=0; i<k; i++) {
            // this will check for random numbers from [ 2, n - 2 ] R, whether R^d
            if (!millerRabinPrimalityTest(n, r, d)) {
                return false;
            }
        }
        return true;
    }

    /**
     * Applies the Miller-Rabin Primality test to n
     * REFERENCE : https://en.wikipedia.org/wiki/Miller–Rabin_primality_test
     * this function is called K times per n, since it's a probabilistic test
     * so the higher K is, the more confident we're that n is a prime.
     *
     * @param n is a number to be tested
     * @param d is the number in n-1 = 2^r * d
     * @param r is the power in n-1 = 2^r * d
     * @return
     *  TRUE: n is probably a prime
     *  FALSE: n is a composite number
     */
    private boolean millerRabinPrimalityTest(BigInteger n, int r, BigInteger d) {
        // Random number in [2 , N - 2]
        BigInteger minRange = BigInteger.valueOf(2);
        BigInteger maxRange = n.subtract(BigInteger.valueOf(2));
        BigInteger a = generateRandomNumberBetween(minRange, maxRange);

        // x = a ^ d % n
        BigInteger x = a.modPow(d, n);

        // if x == 1 or x == n-1, then n is probably a prime
        if (x.compareTo(BigInteger.ONE) == 0 || x.compareTo(n.subtract(BigInteger.ONE)) == 0){
            return true;
        }

        /*
         compute x = (a^2^r)*d % n, for 0<=r<=s-1
         if x == n-1 at any iteration, then n is probably a prime
         */
        for(int i=0; i<r; i++){
            x = x.modPow(BigInteger.valueOf(2), n);

            if (x.compareTo(BigInteger.ONE) == 0) {
                return false;
            }
            if (x.compareTo(n.subtract(BigInteger.ONE)) == 0) {
                return true;
            }
        }
        return false;
    }

    /**
     * Generates a random BigInteger in [minRange - maxRange]
     * @param minRange BigInteger minimum boundary
     * @param maxRange BigInteger maximum boundary
     * @return Random BigInteger
     */
    private BigInteger generateRandomNumberBetween(BigInteger minRange, BigInteger maxRange) {
        Random rnd = new Random();
        int maxNumBitLength = maxRange.bitLength();
        BigInteger range = maxRange.subtract(minRange);
        BigInteger result;

        result = new BigInteger(maxNumBitLength, rnd);

        if (result.compareTo(minRange) < 0)
            result = result.add(minRange);
        if (result.compareTo(maxRange) >= 0)
            result = result.mod(range).add(minRange);
        return result;
    }
}
