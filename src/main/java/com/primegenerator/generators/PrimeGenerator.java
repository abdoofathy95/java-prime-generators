package com.primegenerator.generators;

import java.util.List;

public interface PrimeGenerator {
    /**
     * Generates a list of primes found in the range between s, e
     * NOTE: it's assumed that this range will always fit in heap space, otherwise an error is returned, which should be caught
     * @param s : start of the range (assumed to always be positive)
     * @param e : end of the range (assumed to always be positive and e > s)
     * @return a list of primes or empty list
     */
    List<Long> generatePrimeInRange(long s, long e) throws OutOfMemoryError;
}
