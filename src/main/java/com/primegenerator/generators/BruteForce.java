package com.primegenerator.generators;

import java.util.LinkedList;
import java.util.List;

public class BruteForce implements PrimeGenerator{

    /**
     * Generates a list of primes in the range between s, e by looping over all numbers in that range
     * and checking if a number is prime or not
     * NOTE: it's assumed that this range will always fit in heap space, otherwise an error is returned
     * @param s : start of the range
     * @param e : end of the range
     * @return a list of primes found in range [s - e] if any, empty list otherwise
     */
    @Override
    public List<Long> generatePrimeInRange(long s, long e) {
        List<Long> result = new LinkedList<>();
        if(s > e || e <0 || s < 0) {
            return result;
        }

        s = s == 1 || s == 0 ? 2 : s;

        for (long i = s; i <= e; i++) {
            if (isPrime(i)) {
                result.add(i);
            }
        }
        return result;
    }

    /**
     * Check if a number is divisible by any number in range [2 - sqrt(n)]
     * @param n: suspected number
     * @return whether or not a number is prime
     */
    private static boolean isPrime(long n) {
        if(n == 1 || n < 0) return false;
        for (int i = 2; i < n; i++) {
            if (n % i == 0) {
                return false;
            }
        }
        return true;
    }
}
