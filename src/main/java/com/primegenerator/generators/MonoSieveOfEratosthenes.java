package com.primegenerator.generators;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Vector;

public class MonoSieveOfEratosthenes implements PrimeGenerator {

    /**
     * Generates a list of primes found in the range between s, e
     * @param s : start of the range (assumed to always be positive)
     * @param e : end of the range (assumed to always be positive and e > s)
     * @return a list of primes or empty list
     */
    @Override
    public List<Long> generatePrimeInRange(long s, long e) {
        Vector<Long> result = new Vector<>();
        if(s > e || e <0 || s < 0) {
            return result;
        }
        List<Integer> primes = sieveOfEratosthenes((int) Math.floor(Math.sqrt(e))); // get all primes till sqrt(e)
        s = s == 1 ? 2 : s;
        int [] flag = new int [(int) (e - s)]; // hold range of numbers to be inspected (inclusive)
        long m;
        Arrays.fill(flag, 1);

        for (int prime : primes) {
            m = (s / prime) * prime;
            // loop over range
            while (m < e) {
                // remove all composites
                if (m >= s && ((int) m) != prime) {
                    flag[(int) (m - s)] = 0;
                }
                m += prime;
            }
        }

        for(int i=0; i<flag.length; i++) {
            if(flag[i] == 1){
                result.add(i+s);
            }
        }
        return result;
    }

    /**
     * Implements Monolithic Sieve of Eratosthenes (one array is used for the whole range)
     * Generates a list of primes up to n, by doing the following:
     *  1. store numbers from [2 - n] in an array,
     *  2. mark all numbers in the array
     *  3. loop from i=2 to i=sqrt(n), each marked number is a prime -> store it and do step 4
     *  4. unmark all numbers that are multiples of the current number in x steps, where x is the current number
     *      for example if current number is 2, then unmark all multiples of 2 in a loop with a step of 2, so 4,6,8,10,... are unmarked
     *      same for 3, in steps of 3s so 6,9,12,15,... are unmarked
     *  5. any left marked numbers are the primes number up to n
     *
     *  NOTE: this algorithm uses O(n) in memory and is highly limited by heap size available
     * @param n: range till which the algorithm generates primes
     * @return List of all prime numbers up to n
     */
    private static List<Integer> sieveOfEratosthenes(int n) {
        int [] flag = new int [n + 1];

//        int l = 0; // to count number of primes generated
        Arrays.fill(flag, 1); // mark all numbers

        for(int i=2; i<flag.length; i++) {
            if(flag[i] == 1) {
                int j = i * 2;
//                l++;
                // remove multiples of the prime number (composite numbers)
                while(j < flag.length){
                    flag[j] = 0;
                    j+=i;
                }
            }
        }

        List<Integer> result = new LinkedList<>();

        // add remaining marked numbers to the primes result list
        for(int i=2; i<flag.length; i++){
            if(flag[i] == 1){
                result.add(i);
            }
        }

        return result;
    }
}
