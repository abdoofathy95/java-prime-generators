package com.primegenerator.generators;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Vector;

public class SegmentedSieveOfEratosthenes implements PrimeGenerator {

    /** Implements Segmented Sieve of Eratosthenes
     * Generates a list of primes found in the range between s, e
     * The algorithm goes as follows
     *  1. calculate all the primes up to sqrt(e) using monolithic sieve
     *  2. split the range [s - e] into at most sqrt(e) chunks
     *  3. apply the sieving to every chunk
     *  4. append the resulting primes (checked numbers) to a list
     * @param s : start of the range (assumed to always be positive)
     * @param e : end of the range (assumed to always be positive and e > s)
     * @return a list of primes or empty list
     */
    @Override
    public List<Long> generatePrimeInRange(long s, long e) {
        Vector<Long> result = new Vector<>();
        if(s > e || e <0 || s < 0) {
            return result;
        }
        int segmentSize = (int) Math.floor(Math.sqrt(e));
        List<Integer> primes = sieveOfEratosthenes(segmentSize); // get all primes till sqrt(e)
        s = s == 1 ? 2 : s;
        long segmentStart = s; // start of the segment
        long segmentEnd = segmentStart + segmentSize; // end of a segment

//        int l = 0; // count the number of primes generated
        // process segment at a time
        while(segmentStart < e) {
            if(segmentEnd > e) {
                segmentEnd = e;
            }

            int [] flag = new int [(int) (segmentEnd - segmentStart)]; // hold range of numbers to be inspected (inclusive)
            long m;
            Arrays.fill(flag, 1);

            for (int prime : primes) {
                m = (segmentStart / prime) * prime;
                // loop over range
                while (m < segmentEnd) {
                    // remove all composites
                    if (m >= segmentStart && ((int) m) != prime) {
                        flag[(int) (m - segmentStart)] = 0;
                    }
                    m += prime;
                }
            }

            for(int i=0; i<flag.length; i++) {
                if(flag[i] == 1){
                    result.add(i+segmentStart);
//                    l++;
                }
            }


            segmentStart += segmentSize;
            segmentEnd += segmentSize;
        }

        return result;
    }


    /**
     * Implements Monolithic Sieve of Eratosthenes (one array is used for the whole range)
     * Generates a list of primes up to n, by doing the following:
     *  1. store numbers from [2 - n] in an array,
     *  2. mark all numbers in the array
     *  3. loop from i=2 to i=sqrt(n), each marked number is a prime -> store it and do step 4
     *  4. unmark all numbers that are multiples of the current number in x steps, where x is the current number
     *      for example if current number is 2, then unmark all multiples of 2 in a loop with a step of 2, so 4,6,8,10,... are unmarked
     *      same for 3, in steps of 3s so 6,9,12,15,... are unmarked
     *  5. any left marked numbers are the primes number up to n
     *
     *  NOTE: this algorithm uses O(n) in memory and is highly limited by heap size available
     * @param n: range till which the algorithm generates primes
     * @return List of all prime numbers up to n
     */
    private static List<Integer> sieveOfEratosthenes(int n) {
        int [] flag = new int [n + 1];

        int l = 0; // to count number of primes generated
        Arrays.fill(flag, 1); // mark all numbers

        for(int i=2; i<flag.length; i++) {
            if(flag[i] == 1) {
                int j = i * 2;
                l++;
                // remove multiples of the prime number (composite numbers)
                while(j < flag.length){
                    flag[j] = 0;
                    j+=i;
                }
            }
        }

        List<Integer> result = new LinkedList<>();

        // add remaining marked numbers to the primes result list
        for(int i=2; i<flag.length; i++){
            if(flag[i] == 1){
                result.add(i);
            }
        }

        return result;
    }
}
