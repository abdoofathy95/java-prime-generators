package com.primegenerator.generators;

public enum PrimeGeneratorAlgorithms {
    BF, MR, MSOE, SSOE
}

