package com.primegenerator.generators;

public class PrimeGeneratorFactory {
    public PrimeGenerator get(String methodName) {
        switch(methodName){
            case "BF": return new BruteForce();
            case "MR": return new MillerRabin();
            case "MSOE": return new MonoSieveOfEratosthenes();
            case "SSOE": return new SegmentedSieveOfEratosthenes();
            default: return new BruteForce();
        }
    }
}
