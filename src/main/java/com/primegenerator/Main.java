package com.primegenerator;


import com.primegenerator.generators.PrimeGenerator;
import com.primegenerator.generators.PrimeGeneratorAlgorithms;
import com.primegenerator.generators.PrimeGeneratorFactory;

public class Main {

    public static void main(String args[]) {
        if(args.length != 6) {
            System.out.println
                    (
                    "Usage: \t PrimeGenerator -s START_FROM -e END_AT -a ALG \n\n"+
                            "\t START_FROM: Positive Number, END_AT: Positive > START_FROM\n"+
                            "\t ALG is one of the following: \n\n"+
                            "\t BF: Brute Force \n"+
                            "\t MR: Miller-Rabin \n"+
                            "\t MSOE: Monolithic Sieve of Eratosthenes (One Array for the whole range) \n"+
                            "\t SSOE: Segmented Sieve of Eratosthenes \n\n"+
                            "\t for example: PrimeGenerator -s 1 -e 50 -a BF"
                    );
        } else {

            if(validInputs(args[1], args[3], args[5])) {
                int s = Integer.parseInt(args[1]);
                int e = Integer.parseInt(args[3]);
                PrimeGeneratorFactory f = new PrimeGeneratorFactory();
                PrimeGenerator primeGenerator = f.get(args[5]);
                System.out.println(primeGenerator.generatePrimeInRange(s, e));
            }
        }
    }

    private static boolean validInputs(String s, String e, String alg) {
        boolean found = false, valid = true;
        for (PrimeGeneratorAlgorithms a : PrimeGeneratorAlgorithms.values()) {
            if (a.name().equalsIgnoreCase(alg))
                found = true;
        }
        if(!found) {
            System.out.println(
            "\t ALG is one of the following: \n\n"+
            "\t BF: Brute Force \n"+
            "\t MR: Miller-Rabin \n"+
            "\t MSOE: Monolithic Sieve of Eratosthenes (One Array for the whole range) \n"+
            "\t SSOE: Segmented Sieve of Eratosthenes"
            );
        }
        try{
            int intS = Integer.parseInt(s);
            int intE = Integer.parseInt(e);
            if(intS > intE){
                valid = false;
                System.out.println(
                "S must be smaller than E & both must be valid positive integers"
                );
            }
        }catch(NumberFormatException err){
            valid = false;
            System.out.println(
            "S & E Both must be valid positive integers"
            );
        }
        return found && valid;
    }
}

