package tests;

import com.primegenerator.generators.PrimeGenerator;
import com.primegenerator.generators.SegmentedSieveOfEratosthenes;
import org.junit.jupiter.api.Test;

import java.math.BigInteger;
import java.util.LinkedList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class SegmentedSieveOfEratosthenesTest {

    private List<Long> list = null;
    private PrimeGenerator primeGenerator = new SegmentedSieveOfEratosthenes();
    private List<Long> emptyList = new LinkedList<>();

    @Test
    void generatePrimesInvalidRange() {
        list = primeGenerator.generatePrimeInRange(10L , 0L);
        assertArrayEquals(emptyList.toArray(), list.toArray());
        list = primeGenerator.generatePrimeInRange(0L , 0L);
        assertArrayEquals(emptyList.toArray(), list.toArray());
        list = primeGenerator.generatePrimeInRange(-10L , -5L);
        assertArrayEquals(emptyList.toArray(), list.toArray());
    }


    @Test
    void generatePrimesInRange() {
        list = primeGenerator.generatePrimeInRange(1L , 1000L);
        assertTrue(numbersArePrime(list));
        list = primeGenerator.generatePrimeInRange(10L , 500L);
        assertTrue(numbersArePrime(list));
    }

    private boolean numbersArePrime(List<Long> list) {
        BigInteger bigInteger;

        for (Long aL : list) {
            bigInteger = BigInteger.valueOf(aL);
            if(!bigInteger.isProbablePrime(128)) {
                return false;
            }
        }
        return true;
    }
}