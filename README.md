# Java-Prime-Generator

### To send a generate request:
[POST] @ http://207.154.200.17/prime_generator/api/generate

{
	"s": START_OF_RANGE,
	"e": END_OF_RANGE,
	"alg": ALGORITHM_NAME
}

Where
1 <= S < E <= 2^32-1 & alg ∈ ("BF", "MR", "MSOE", "SSOE")


BF: brute force

MR: miller-rabin

MSOE: monolithic sieve of eratosthenes 

SSOE: segmented sieve of eratosthenes


### To see stats:
[GET] @ http://207.154.200.17/prime_generator/api/stats
